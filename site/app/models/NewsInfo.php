<?php

namespace App\Models;

use Phalcon\Mvc\Model;

class NewsInfo extends Model
{

    public function afterFetch()
    {
        $text = str_replace("\n", "<br>", $this->text);
        $text = preg_replace('~(https?://[\w\/?=#-_]*)~', '<a href="$1" target="_blank" class="text_link">$1</a>', $text);
        $this->modifyText = preg_replace('~(www.[\w\/?=#-_]*)~', '<a href="http://$1" target="_blank" class="text_link">$1</a>', $text);
    }

}
