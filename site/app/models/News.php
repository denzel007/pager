<?php

namespace App\Models;

use Phalcon\Mvc\Model;

class News extends Model
{

    public function initialize()
    {
        $this->hasOne('id', 'NewsInfo', 'newsId', array('alias' => 'info'));
        $this->hasMany('id', 'NewsInfo', 'newsId', array('alias' => 'allInfo'));
    }

    public function afterFetch()
    {
        $this->dateCreatedStr = strtoupper(date('d M Y', strtotime($this->dateCreated)));
        $this->dateUpdatedStr = strtoupper(date('d M Y', strtotime($this->dateUpdated)));
    }

     /**
     * Upload or remove image
     * 
     * @param Request $request upload request
     * @param string $imageAction Action name
     * @return boolean
     */
    public function uploadPhoto($request, $imageAction)
    {
        if ($imageAction == 'update') {
           $imgData = UploadHelper::uploadImage($request); 
        } else if ($imageAction == 'delete') {
            $imgData = UploadHelper::deleteImage($this->imageUrl);  
        }
        $this->aspectRatio = ($imgData[0]['aspectRatio']) ? $imgData[0]['aspectRatio'] :  0;
        $this->imageUrl = ($imgData[0]['imageUrl']) ?  $imgData[0]['imageUrl'] : '';   
        return true;
    }
}
