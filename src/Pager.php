<?php

namespace Tetrapak07\Pager;

use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use Phalcon\Mvc\Controller;
use stdClass;
use Supermodule\ControllerBase as SupermoduleBase;

class Pager extends Controller
{
      
    public function initialize()
    {
        $this->dataReturn = true;
        if (!SupermoduleBase::checkAndConnectModule('pager')) {

           //header("Location: /index/show404");
           //exit;
            $this->dataReturn = false;
        }   
    }
    
    public function onConstruct()
    {
       
    }

    public function paginator($content, $contentType = '', $limit = false, $currentPage = 1)
    {
        $data = false;
        if (!$this->dataReturn) {
            return $data;
        }
        $lim = (!$limit) ? $this->config->modules->pager->limit : $limit;
        $paginator = new PaginatorModel(array(
            'data' => $content,
            'limit' => $lim,
            'page' => $currentPage
        ));
        
        $data = new stdClass; 
        $data->$contentType = $paginator->getPaginate();
        $data->total = $content->count();
        
        return $data;
    }
    
}
